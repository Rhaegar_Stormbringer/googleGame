from tkinter import *
from classes import *
from random import *

class Gui():

	def __init__(self, lst_keys, W=800, H=600):
		self.root = Tk()
		self.width = W
		self.height = H
		self.canvas = Canvas(self.root, width=self.width, height=self.height)
		self.canvas.pack()
		self.root.update()
		
		self.rez = Rez(self.canvas)
		self.root.title("googlic park")

		self.inputs = lst_keys
		self.keyDown = {}
		self.setBindings()
		
		self.mouseFocus = None
		self.mousePressed = [False, False]

		self.game = False
		self.Y = (self.height - self.rez.Y6)
		self.tick = 0
		self.gravity = 1
		self.distance = 0
		self.highscore = 0
		self.text = [(UIText(self.canvas, self.rez.X1-self.rez.X10, self.rez.Y10, font="Arial 20", text="00000", value=self.distance, instantgenerate=True), "distance"),
					(UIText(self.canvas, self.rez.X1-3*self.rez.X10, self.rez.Y10, font="Arial 20", text="HI 00000", value=self.highscore, instantgenerate=True), "highscore")]
		self.gameStatus_text = UIText(self.canvas, self.rez.X2, self.rez.Y2-self.rez.Y10, font ="Arial 20", text="Press to Play", instantgenerate=True)

		self.player = Dinosaur(self.canvas)
		
		self.button = UIElement(self, self.canvas, [self.startGame, self.stopGame], self.rez.X12, self.rez.X12, "menu", self.rez.X2, self.rez.Y2)

		self.lst_obs = []
		self.lst_obs_waiting = []
		self.lst_obs_alive = []

	def start(self):
		self.setUI()
		for i in range(10):
			o1 = Obstacle(self.canvas, "cactus")
			o2 = Obstacle(self.canvas, "little cactus")
			o3 = Obstacle(self.canvas, "group cactus")
			o4 = Obstacle(self.canvas, "big cactus")
			self.lst_obs.append(o1)
			self.lst_obs.append(o2)
			self.lst_obs.append(o3)
			self.lst_obs.append(o4)
			self.lst_obs_waiting.append(o1)
			self.lst_obs_waiting.append(o2)
			self.lst_obs_waiting.append(o3)
			self.lst_obs_waiting.append(o4)
		for i in range(8):
			o5 = Obstacle(self.canvas, "bird")			
			self.lst_obs.append(o5)			
			self.lst_obs_waiting.append(o5)

	
		self.player.generate(self.rez.X8, self.Y, "bottom")
		self.lst_obs[0].generate(self.rez.X2+self.rez.X4, self.Y, p="bottom")
		self.lst_obs_waiting.remove(self.lst_obs[0])
		self.lst_obs_alive.append(self.lst_obs[0])

		self.refresh()

	def mainloop(self):	
		self.root.mainloop()

	def startGame(self):
		self.gameStatus_text.die()
		self.button.die()
		# -- reset all objects -- #
		self.player.die()
		self.player.generate(self.rez.X8, self.Y, "bottom")
		self.lst_obs_waiting = []
		for o in self.lst_obs_alive:
			o.die()
		self.lst_obs_alive = []
		for o in self.lst_obs:
			self.lst_obs_waiting.append(o)
		self.lst_obs[0].generate(self.rez.X2+self.rez.X4, self.Y, p="bottom")
		self.lst_obs_waiting.remove(self.lst_obs[0])
		self.lst_obs_alive.append(self.lst_obs[0])
		
		self.tick = 0
		self.distance = 0
		self.updateDistance()
			
		self.game = True

	def stopGame(self):
		self.game = False

	def refresh(self):
		if (self.game):
			if self.keyDown["space"]: self.player.jump(0, -24, self.gravity, True)
			elif not self.keyDown["space"]: self.player.jump(0, -12, self.gravity, False)	
			
			if self.keyDown["s"]: self.player.crouch(True)
			elif not self.keyDown["s"]: self.player.crouch(False)
		
			
			self.gameLoop()
			self.root.update()
			self.tick += 1
		self.root.after(17, self.refresh)

	def setBindings(self):
		self.root.bind("<Motion>", self.mouseMoved)
		self.root.bind("<ButtonPress-1>", self.mouse1Pressed)
		self.root.bind("<ButtonRelease-1>", self.mouse1Released)
		for keysym in self.inputs:
			self.root.bind("<KeyPress-%s>" %keysym, self.keyPressed)
			self.root.bind("<KeyRelease-%s>" %keysym, self.keyReleased)
			self.keyDown[keysym] = False

	def mouseMoved(self, event):
		if not (self.mousePressed[0]):
			t = self.canvas.find_overlapping(event.x, event.y, event.x, event.y)
			if (len(t) > 0):
				self.mouseFocus = Obj2D.LST_ALL[t[-1]-1]
				self.mouseFocus.mouseOver() 
			else:
				if (self.mouseFocus != None):
					self.mouseFocus.mouseLeave()
					self.mouseFocus = None

	def mouse1Pressed(self, event):
		t = self.canvas.find_overlapping(event.x, event.y, event.x, event.y)
		if (len(t) >0):
			self.mouseFocus = Obj2D.LST_ALL[t[-1]-1]
			self.mouseFocus.mouse1Pressed()
			self.mousePressed[0] = True

	def mouse1Released(self, event):
		t = self.canvas.find_overlapping(event.x, event.y, event.x, event.y)
		if (len(t) >0):
			if ((self.mouseFocus != None) and (self.mouseFocus == Obj2D.LST_ALL[t[-1]-1])):
				self.mouseFocus.mouse1Released()
				self.mousePressed[0] = False
			elif ((self.mouseFocus != None) and (self.mouseFocus != Obj2D.LST_ALL[t[-1]-1])):
				self.mouseFocus.mouseLeave()
				self.mouseFocus = None
				self.mousePressed[0] = False
		else:
			if (self.mouseFocus != None):
				self.mouseFocus.mouseLeave()
				self.mouseFocus = None
				self.mousePressed[0] = False

	def keyPressed(self, event):
		self.keyDown[event.keysym] = True
	
	def keyReleased(self, event):
		self.keyDown[event.keysym] = False

	def setUI(self):
		self.button.generate(self.button.x, self.button.y)
		#for elmt in self.lst_ui[state]:
		#	elmt.generate(elmt.x, elmt.y)


	def gameLoop(self):
		# --- E1: create new obstacle --- #
		if (self.lst_obs_alive[-1].center[0] < self.rez.X2+self.rez.X4):
			self.randomCreation()

		# --- E2: move obstacles --- #
		for obs in self.lst_obs_alive:
			if (obs.alive):
				obs.move(obs.speed[0], obs.speed[1])
				if obs.center[0] < (-self.rez.X10):
					obs.die()
					self.lst_obs_alive.remove(obs)
					self.lst_obs_waiting.append(obs)
		
		# --- E3: check for player's collisions --- #
		c = self.player.collide()
		if (c[0]):
			self.gameOver()

		# --- E4; increase distance and update text --- #
		self.updateDistance()
		self.updateHighscore()
	
	def gameOver(self):
		self.stopGame()
		self.button.reset()
		self.setUI()
		self.gameStatus_text.changeText("Game Over !")
		self.gameStatus_text.generate()



	def randomCreation(self):
		r = randint(0, len(self.lst_obs_waiting)-1)
		d = randint(int(self.rez.X2), int(self.rez.X2+self.rez.X8))
		o = self.lst_obs_waiting[r]
		if (o.kind == "bird"):
			o.generate(self.lst_obs_alive[-1].center[0]+d, self.Y-self.rez.Y10, p="bottom")
		elif ("cactus" in o.kind):
			o.generate(self.lst_obs_alive[-1].center[0]+d, self.Y, p="bottom")
		self.lst_obs_waiting.remove(o)
		self.lst_obs_alive.append(o)


	def updateDistance(self):
		self.distance = int(self.tick*0.017)
		self.text[0][0].value = self.distance
		self.updateText("distance")


	def updateHighscore(self):
		if (self.distance > self.highscore):
			self.highscore = self.distance
			self.text[1][0].value = self.highscore
			self.updateText("highscore")


	##
	# updateText(textToken, newText="")
	# Method used to update the text on one of the text for the scores displayed
	# on the screen. The **textToken** parameter is used to determine which text
	# need to be updated, and **newTextStart** is used as starting string for
	# this new text
	##
	def updateText(self, textToken, newTextStart=""):
		for e in self.text:
			if (textToken in e):
				s = str(e[0].value)
				newText = newTextStart
				for i in range(0, (5-len(str(e[0].value)))):
					newText += "0"
				newText += s
				e[0].changeText(newText)
				break
