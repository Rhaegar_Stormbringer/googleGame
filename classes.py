##################################################################################################
#					GOOGLIC PARK						 #
#			       made by : Rhaegar Stormbringer					 #
#				         May 2019						 #
#------------------------------------------------------------------------------------------------#
# This game is a remake of the google "dinosaur game" - also called with other names like	 #
# "dinosaur game google", "chrome dinosaur game", "offline dinosaur game", "no wifi dinosaur	 #
# game", and "t rex game"), a simple infinite runner game that you can play on chrome in offline #
# mode. This remake was programmed entirely in python by using the interface to the Tk GUI	 #
# toolkit Tkinter. This remake is nothing more than a personal project made on free time to	 #
# apprehend some aspects of video game programming. Not a single profit was or will be made	 #
# using this project. All credits goes to Google LLC and the original design team of the game,	 #
# Edward Jung, Sebastien Gabriel and Alan Bettes						 #
##################################################################################################



##################################################################################################
#
#
#
##################################################################################################

###########################################################
# Obj2D()
#
###########################################################
class Obj2D():

	LST_ALL = []
	LST_ALIVE = []

	def __init__(self, canvas, width, height, color="green", colorOver="green3", colorPressed="green2"):
		self.id = canvas.create_rectangle(-1, -1, -1, -1, fill=color)

		self.width = width
		self.height = height
		self.color = color
		self.colorOver = colorOver
		self.colorPressed = colorPressed

		self.canvas = canvas
		self.center = [0, 0]
		self.canvas.itemconfig(self.id, state="hidden")
		
		self.alive = False
		Obj2D.LST_ALL.append(self)

	
	##
	# generate(x, y, p)
	# Method used to move the object at the position (x, y), and make it visible
	# x and y are the new coordinate of the **center** of the shell. If one of them
	# is a negative, it get replaced by the associated center sub-attribute:
	# this allow the object to be regenerated at the same coordinates by calling
	# the generation method without any parameters. p is the "direction" of the 
	# generation: it temporaly shift the center of the object and the dimension
	# of the applied width and height to make it "grow" following a direction (for
	# exemple, from top to bottom). In the rest of the documentation, p is called
	# **direction vector**
	##
	def generate(self, x=-1, y=-1, p="center"):
		if (x < 0):
			x = self.center[0]
		if (y < 0):
			y = self.center[1]
		if (p == "center"):
			self.canvas.coords(self.id, x-(self.width/2), y-(self.height/2), x+(self.width/2), y+(self.height/2))
		elif (p == "bottom"):
			self.canvas.coords(self.id, x-(self.width/2), y-self.height, x+(self.width/2), y)
			y = (y-self.height/2)

		self.canvas.itemconfig(self.id, state="normal")
		self.center = [x, y]

		self.alive = True
		Obj2D.LST_ALIVE.append(self)

	##
	# die()
	# Method used to hide the object, simulating its death
	# It also move it to (-1, -1) for collision purpose : as much as the hidden
	# state of tkinter is efficient, it give a double security on that matter
	# Note that the object is moved but its center attribute is not refreshed:
	# this allow the object to be regenerated at the same coordinates by calling
	# the generation method without any parameters.
	##
	def die(self):
		self.canvas.itemconfig(self.id, state="hidden")
		self.canvas.coords(self.id, -1, -1, -1, -1)

		self.alive = False
		Obj2D.LST_ALIVE.remove(self)


	##
	# resize(nW, nH, p)
	# Method used to deform the object, and redraw it given a new width nW and
	# a new heigh nH, and a new direction vector p *(see the metho **generate** for
	# more information about p)*. Note that this method doesn't check if the object
	# is alive
	def resize(self, nW, nH, p="center"):
		p = p.lower()
		if (nW < 0):
			nW = self.width
		if (nH < 0):
			nH = self.height
		if (p == "center"):
			self.canvas.coords(self.id, self.center[0]-(nW/2), self.center[1]-(nH/2), self.center[0]+(nW/2), self.center[1]+(nH/2))
		elif (p == "bottom"):
			self.canvas.coords(self.id, self.center[0]-(nW/2), self.center[1], self.center[0]+(nW/2), self.center[1]+(self.height/2))
		self.width, self.height = nW, nH


	##
	# move(vX, vY)
	# Method used to move the object on the screen by making the translation
	# (center[0] + vX, center[1] + vY). Note that this method doesn't check if
	# the object is alive.
	##
	def move(self, vX, vY):
		nC = [self.center[0] + vX, self.center[1] + vY]
		self.canvas.coords(self.id, nC[0]-(self.width/2), nC[1]-(self.height/2), nC[0]+(self.width/2), nC[1]+(self.height/2))
		self.center = nC


	##
	# collide()
	# Method used to check if the object has a collision with another object on the screen,
	# by returning a tuple (boolean, id) where :
	# **boolean** is the boolean indicating if a collision with **another** object occured or not
	# **id** is the id of the object itself (due to the way tkinter.canvas.find_overlapping() works), or
	# the id of the other object. The method doesn't use the attributes of the object because
	# of the possibility of a temporary resizing, and rather use the canvas.coords() method
	# to retrieve real object coordinates. Note that this method doesn't check if the object
	# is alive.
	##
	def collide(self):
		self.canvas.tag_lower(self.id)
		co = self.canvas.coords(self.id)
		c = self.canvas.find_overlapping(co[0], co[1], co[2], co[3])
		return (len(c) > 1, c[-1])
		

	##
	# mouse1Pressed()
	# Method used whenever the button 1 of the mouse is pressed on the object (in
	# googlic Park, it change the color of the object). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouse1Pressed)
	##
	def mouse1Pressed(self):
		self.canvas.itemconfig(self.id, fill=self.colorPressed)


	##
	# mouse1Released()
	# Method used whenever the button 1 of the mouse is released on the object (in
	# googlic Park, it change the color of the object). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouse1Released)
	##
	def mouse1Released(self):
		self.canvas.itemconfig(self.id, fill=self.colorOver)

	
	##
	# mouseOver()
	# Method used whenever the pointer of the mouse pass over the object (in
	# googlic Park, it change the color of the object). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouseOver)
	##
	def mouseOver(self):
		self.canvas.itemconfig(self.id, fill=self.colorOver)


	##
	# mouseLeave()
	# Method used whenever the pointer of the mouse leave the object (in
	# googlic Park, it change the color of the object). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouseLeave)
	##
	def mouseLeave(self):
		self.canvas.itemconfig(self.id, fill=self.color)



##################################################################################################
# USER INTERFACE (UI)
#
#
##################################################################################################

###########################################################
# UI()
#
###########################################################
class UI():

	def __init__(self, categories):
		self.categories = categories
		self.lst_elmt = {}
		for c in self.categories:
			self.lst_elmt[c] = []

	def addElmt(self, elmt):
		self.lst_elmt[elmt.category].append(elmt)



###########################################################
# UIElement(Obj2D)
#
###########################################################
class UIElement(Obj2D):

	def __init__(self, gui, canvas, functions, width, height, category, x=-1, y=-1, text=None, color="blue"):
		self.gui = gui
		self.canvas = canvas
		self.functions = functions
		self.category = category
		self.x = x
		self.y = y
		self.state = 0

		super().__init__(canvas, width, height, color)
	
	## 
	# reset()
	# Method used to reset the value of the state attribute : the next function called
	# when this UI element will be called will then be the first function associated 
	# with this UI element.
	##
	def reset(self):
		self.state = 0

	##
	# mouse1Pressed()
	# Method used whenever the button 1 of the mouse is pressed on the UI element. This
	# action activate the function associated with this UI element and corresponding to
	# the current state of it. The position of the mouse regarding the object **isn't**
	# checked here: it need to be done in the corresponding gui class method
	# (gui.mouse1Pressed)
	##
	def mouse1Pressed(self):
		self.functions[self.state]()
		self.state = (self.state+1)%(len(self.functions))


###########################################################
# UI_TEXT(Obj2D)
#
###########################################################
class UIText(Obj2D):
	
	def __init__(self, canvas, x="-1", y="-1", font="Arial 12", text="No_text", value=None, instantgenerate=False):
		self.id = canvas.create_text(x, y, font=font, text=text)
		self.canvas = canvas
		self.center = [x, y]
		self.font = font
		self.text = text
		self.value = value
		self.alive = False

		self.canvas.itemconfig(self.id, state="hidden")

		if (instantgenerate):
			self.generate(x, y)
	
		Obj2D.LST_ALL.append(self)

	##
	# generate(x, y, p)
	# Method used to move the object at the position (x, y), and make it visible.
	# Similar to the one of the Obj2D class
	##
	def generate(self, x=-1, y=-1, p="center"):
		if (x < 0):
			x = self.center[0]
		if (y < 0):
			y = self.center[1]
		if (p == "center"):
			self.canvas.coords(self.id, self.center[0], self.center[1])
			self.canvas.itemconfig(self.id, state="normal")
		self.alive = True
		Obj2D.LST_ALIVE.append(self)
	
	##
	# die()
	# Method used to hide the object, simulating its death. Similar to the one
	# of the Obj2D class
	##
	def die(self):
		self.canvas.itemconfig(self.id, state="hidden")
		self.canvas.coords(self.id, -1, -1)

		self.alive = False
		Obj2D.LST_ALIVE.remove(self)

	
	##
	# changeText(nT)
	# Method used to change the text displayed by the object. This method doesn't
	# regenerate the object so a longer newt text nT can get out of the screen
	##
	def changeText(self, nT):
		self.text = nT
		self.canvas.itemconfig(self.id, text=self.text)

	##
	# changeFont
	# Method used to change the font used by the text, as well as the font size. The
	# new font nF **isn't** automatically installed by this function nor this object:
	# an error will occur if the font isn't previously installed. This method doesn't
	# regenerate the object so a bigger font size can get the text out of the screen
	## 
	def changeFont(self, nF):
		self.font = nF
		self.canvas.itemconfig(self.id, font=self.font)

	##
	# mouse1Pressed()
	# Method used whenever the button 1 of the mouse is pressed on the object (in
	# googlic Park, it does nothing). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouse1Pressed)
	##
	def mouse1Pressed(self):
		return None


	##
	# mouse1Released()
	# Method used whenever the button 1 of the mouse is released on the text (in
	# googlic Park, it does nothing). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouse1Released)
	##
	def mouse1Released(self):
		return None

	
	##
	# mouseOver()
	# Method used whenever the pointer of the mouse pass over the object (in
	# googlic Park, it does nothing). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouseOver)
	##
	def mouseOver(self):
		return None


	##
	# mouseLeave()
	# Method used whenever the pointer of the mouse leave the object (in
	# googlic Park, it does nothing). The position of the mouse
	# regarding the object **isn't** checked here: it need to be done in the
	# corresponding gui class method (gui.mouseLeave)
	##
	def mouseLeave(self):
		return None




##################################################################################################
# GAMEPLAY ELEMENTS
#
#
##################################################################################################

###########################################################
# Obstacle(Obj2D)
#
###########################################################
class Obstacle(Obj2D):

	def __init__(self, canvas, kind):
		self.kind = kind
		self.speed = (-1, 0)

		width = 0
		height = 0
		color = "green"
		
		if (kind == "cactus"):
			width = canvas.winfo_width()/24
			height = canvas.winfo_height()/6
			color = "green"
			self.speed = (-8, 0)
		if (kind == "little cactus"):
			width = canvas.winfo_width()/24
			height = canvas.winfo_height()/10
			color = "green"
			self.speed = (-8, 0)
		if (kind == "group cactus"):
			width = canvas.winfo_width()/8
			height = canvas.winfo_height()/6
			color = "green"
			self.speed = (-8, 0)
		if (kind == "big cactus"):
			width = canvas.winfo_width()/24
			height = canvas.winfo_height()/4
			color = "green"
			self.speed = (-8, 0)
		if (kind == "bird"):
			width = canvas.winfo_width()/24
			height = canvas.winfo_height()/16
			color = "blue"
			self.speed = (-6, 0)

		super().__init__(canvas, width, height, color)


###########################################################
# Dinosaur(Obj2D)
#
###########################################################
class Dinosaur(Obj2D):

	def __init__(self, canvas, life = 1, color="red"):
		self.life = life
		self.speed = [0, 0]
		self.jumping = False
		self.crouching = False

		super().__init__(canvas, canvas.winfo_width()/20, canvas.winfo_height()/6, color)

		self.y = self.center[1]
		self.h = self.height

	
	##
	# jump(vX, vY, g, b)
	# Method used to make the object jump. When this method is called, multiple
	# booleans are checked :
	# * jumping, as the object can't jump if its already in the air
	# * crouching, as the object can't crouch and jump at the same time
	# * b, the parameter, that correspond to the player's input (b is True mean that
	#   the player downed the jump key)
	# If the boolean b is **True**, then the vertical position of the object is saved
	# in a temporary variable, corresponding to the y attribute, and then an initial
	# vectorial impulsion (vX, vY) is applied to the object. From there, the gravity
	# g is applied to the object's velocity each frame, which will eventually fall
	# under its effect. The falling phase last until the object has find its initial
	# vertical position y again.
	##
	def jump(self, vX, vY, g, b):
		if ((self.jumping == False) and (self.crouching == False) and (b == True)):
			self.speed = [vX, vY]
			self.y = self.center[1]
			self.jumping = True
		elif ((self.jumping == True) and (self.crouching == False) and (b == True)):
			self.speed[1] += g
			self.move(self.speed[0], self.speed[1])
			if (self.y < self.center[1]):
				self.jumping = False
				self.generate(self.center[0], self.y)

		elif ((self.jumping == True) and (self.crouching == False) and (b == False)):
			self.speed[1] += g
			if (self.speed[1] < vY):
				self.speed[1] = vY
			self.move(self.speed[0], self.speed[1])
			if (self.y < self.center[1]):
				self.jumping = False
				self.generate(self.center[0], self.y)


	##
	# crouch(b)
	# Method used to make the object crouch. When this method is called, multiple
	# booleans are checked :
	# * jumping, as the object can't crouch and jump at the same time
	# * crouching, to know if the object is already crouching
	# * b, the parameter, that correspond to the player's input (b is True mean that
	#   the player downed the crouch key)
	# If the boolean b is **True**, then the height of the object is saved in a temporary
	# variable, corresponding to the h attribute, and then divided by 2. If b is **False**,
	# then the object retrieve its original height. In the two cases, the Obj2D.resize
	# method is called, with a direction vector p="bottom". In any other cases, nothing
	# append, so the object stay in its crouching (or standing) state as long as b didn't
	# change its value.
	##
	def crouch(self, b):
		if ((self.crouching == False) and (self.jumping == False) and (b == True)):
			self.h = self.height
			self.resize(-1, self.height/2, p="bottom")
			self.crouching = True
		elif ((self.crouching == True) and (self.jumping == False) and (b == False)):
			self.resize(-1, self.h)
			self.crouching = False



##################################################################################################
# MISCELLANEOUS
#
#
##################################################################################################

###########################################################
# Rez()
#
###########################################################
class Rez():

	def __init__(self, canvas):
		self.canvas = canvas

		self.X1 = self.canvas.winfo_width()
		self.X2 = self.canvas.winfo_width()/2
		self.X4 = self.canvas.winfo_width()/4
		self.X6 = self.canvas.winfo_width()/6
		self.X8 = self.canvas.winfo_width()/8
		self.X10 = self.canvas.winfo_width()/10
		self.X12 = self.canvas.winfo_width()/12
		self.Y1 = self.canvas.winfo_height()
		self.Y2 = self.canvas.winfo_height()/2
		self.Y4 = self.canvas.winfo_height()/4
		self.Y6 = self.canvas.winfo_height()/6
		self.Y8 = self.canvas.winfo_height()/8
		self.Y10 = self.canvas.winfo_height()/10
		self.Y12 = self.canvas.winfo_height()/12
	

	##
	# addRez(axe, value)
	# Method used to add a new screen division value to the object. This new value
	# is obtained by dividing the canvas resoluion corresponding to the **axe** (X or Y) 
	# argument by the **value** argument. An error occurs if the axe argument is neither
	# X nor Y
	##
	def addRez(self, axe, value):
		if ((axe in ["X", "Y"]) and (isinstance(value, int))):
			if (axe == "X"):
				setattr(self, axe+str(value), self.canvas.winfo_width()/value)
			elif (axe == "Y"):
				setattr(self, axe+str(value), self.canvas.winfo_height()/value)
		else:
			print("[Rez] ERROR 001 : the axe should be either 'X' or 'Y' and value should be a integer")
