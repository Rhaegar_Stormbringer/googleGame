import tensorflow as tf

model = tf.keras.Sequential()

# input layer
model.add(layers.Dense(10, activation='relu', input_shape=[len(trainSet)]))
# hidden layer
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(64, activation='relu'))
# output layer
model.add(layers.Dense(10, activation='softmax'))
