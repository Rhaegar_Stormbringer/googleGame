import time
import os
import glob


def cleanString(string):
	string = string.replace("\n", "")
	string = string.replace("\t", "")
	string = string.replace("# ", "")

	return string


lst_pyFiles = glob.glob('classes.py')
for py in lst_pyFiles:
	print("Generating doc for",py," ...\n")	
	with open(py, "r") as f:
		content = f.readlines()
		while (content != []):
			if ("##" in content.pop(0)):
				h = []
				while not ("##" in content[0]):
					h.append(content.pop(0))
				content.pop(0)

				for k in range(len(h)):
					h[k] = cleanString(h[k])

				print(h,"\n")
				
				#writing the help in a file in the doc directory

				with open("./doc/"+py+".html", 'w') as g:
					for line in h:
						g.write(line)

	print("----- [END",py,"] -----\n")


'''
def clean_string(string):
	string = string.replace("# ", "")
	string = string.replace("@", "")
	string = string.replace("\t", "")

	return string

def clean_title(string):
	string = string.replace("*", "")
	string = string.replace("\n", "")
	string = string.replace("/", "@")
	#string = string.replace(" ", "")

	return string

def find_category(help_list):
	cat = ""
	for i in range(len(help_list)):
		if ("|[category" in help_list[i]):
			cat = help_list[i]
			help_list[i] = help_list[i].replace("|", "")
			break
	cat = cat.replace("category :", "")
	cat = cat.replace("|","")
	cat = cat.replace("[","")
	cat = cat.replace("]","")
	cat = cat.replace("\n","")
	cat = cat.replace(" ","")
	return cat


print("generating help ...")
t1 = time()

#searching the main directory
superpath = os.getcwd()
#searching the help directory and delete all files
help_directory = fileManager.fileFinder(superpath, dirname="doc")
for f in os.listdir(help_directory):
	if f.endswith(".md"):
		os.remove(os.path.join(help_directory, f))

#searching the Monika.py file
monika_file_path = fileManager.fileFinder(superpath, filename="Monika.py")

lst_all_help = []

with open(monika_file_path, "r") as monika_file:
	content = monika_file.readlines()
	for i in range(len(content)):
		if ("## @" in content[i]):
			help = []
			j = 0
			while not ("if (" in content[i+j]):
				help.append(content[i+j])
				j += 1
			help.pop(0)
			for k in range(len(help)):
				help[k] = clean_string(help[k])
			help_name = clean_title(help[0])
			lst_all_help.append([find_category(help), help_name])
			
			#writing the help for this functionnality in a file in the justMyHelp directory
			help_path = fileManager.fileFinder(superpath, dirname="justMyHelp")
			help_path = os.path.join(help_path, help_name)

			with open(help_path+".hl", 'w') as help_file:
				for line in help:
					help_file.write(line)


# write the all_help file
lst_all_help.sort()
all_help_path = fileManager.fileFinder(superpath, dirname="doc", filename="__allHelp__.hl")
remember(1, all_help_path, lst_all_help)

t2 = time()
print("done ({:.3f} s)".format((t2-t1)))
'''
